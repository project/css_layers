## Evaluate the use of @layer rule in Drupal context

We now have CSS laysers which could actually solve the "heavy CSS issue": When you override a style in your theme you currantly need to increase specifity. 

Using CSS layers would allow us to use the same Selectors as the base styles and increase specifity only by the layer order. 

For reference [CSS-tricks summarizes @layer very well](https://css-tricks.com/css-cascade-layers/).

**Not supported by browsers** (See below).

## Theory of this module

is to use the defined css layers provided by core.

Which are

 * CSS_BASE = -200
 * CSS_LAYOUT = -100
 * CSS_COMPONENT = 0 (equals CSS_AGGREGATE_DEFAULT = 0)
 * CSS_STATE = 100
 * CSS_THEME = 200

[Reference for AssetResolver](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Asset%21AssetResolver.php/function/AssetResolver%3A%3AgetCssAssets/8.4.x)

As `libraryDiscovery->getLibraryByName()` does not provide the layers, I just used weights for now to chrchive something like: 

```html
<link rel="stylesheet" media="all" href=".../css-layers.css" />
<link rel="stylesheet" media="all" href=".../core.css" layer="layout" />
<link rel="stylesheet" media="all" href=".../coffee.css" layer="component" />
<link rel="stylesheet" media="all" href=".../theme.css" layer="theme" />

```

The modules library just contains basic layer definition:

```CSS
@layer base, layout, component, state, theme;
```
## THE issue with this approach

Sadly CSS layers are currently only supported at the import level. E.g: 

```CSS
@import url(blabla.css) layer(theme);
```

Which we don't use in Drupal currently. Seems we need to wait for that.

* https://bugs.chromium.org/p/chromium/issues/detail?id=1338543
* https://github.com/digitaldonkey/css-layers-test
* https://www.drupal.org/node/1924368 

### Aggregation

Leverage "group" [css-property](https://www.drupal.org/docs/theming-drupal/adding-assets-css-js-to-a-drupal-theme-via-librariesyml#css-properties) to agregate in Groups but with layers?

Look

### Wight or CSS-Category based approach?

Implement using the actual css "layer" named `$category` in LibraryDiscoveryParser::buildByExtension() instead of current weight based approach.

**Potntial "CSS layers"** are defined in xx.libraries.yml.

```yaml
demo:
  css:
    base:
      demo/css/base.css: { minified: false }
    layout:
      demo/css/layout.css: { minified: false }      
    component:      
      demo/css/component.css: { minified: false }
    state:      
      demo/css/state.css: { minified: false }      
    theme:
      demo/css/theme.css: { minified: false }
```

This would require a one liner patch in LibraryDiscoveryParser::buildByExtension() monster function:

```
core/lib/Drupal/Core/Asset/LibraryDiscoveryParser.php:181

// Add category for CSS layers.
$options['layer'] = $category;
```

