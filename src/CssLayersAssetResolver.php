<?php
/**
 * @file
 *
 * Evaluating how we can make use the CSS layers feature in Drupal.
 * @see https://css-tricks.com/css-cascade-layers/
 *
 * Use the defined css layers provided by core.
 * Which are
 *  - CSS_BASE = -200
 *  - CSS_LAYOUT = -100
 *  - CSS_COMPONENT = 0 (equals CSS_AGGREGATE_DEFAULT = 0)
 *  - CSS_STATE = 100
 *  - CSS_THEME = 200
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Asset%21AssetResolver.php/function/AssetResolver%3A%3AgetCssAssets/8.4.x
 */

namespace Drupal\css_layers;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Asset\AssetResolver;
use Drupal\Core\Asset\AssetResolverInterface;
use Drupal\Core\Asset\AttachedAssetsInterface;
use Drupal\Core\Cache\CacheBackendInterface;


class CssLayersAssetResolver extends AssetResolver implements AssetResolverInterface {
  /**
   * {@inheritdoc}
   */
  public function getCssAssets(AttachedAssetsInterface $assets, $optimize) {
    $theme_info = $this->themeManager->getActiveTheme();
    // Add the theme name to the cache key since themes may implement
    // hook_library_info_alter().
    $libraries_to_load = $this->getLibrariesToLoad($assets);
    $cid = 'css:' . $theme_info->getName() . ':' . Crypt::hashBase64(serialize($libraries_to_load)) . (int) $optimize;
    if ($cached = $this->cache->get($cid)) {
      // TODO
      // Note: Comment out for evaluation as only updated on module install.
      return $cached->data;
    }

    $css = [];
    $default_options = [
      'type' => 'file',
      'group' => CSS_AGGREGATE_DEFAULT,
      'weight' => 0,
      'media' => 'all',
      'preprocess' => TRUE,
      'browsers' => [],
    ];

    foreach ($libraries_to_load as $library) {
      [$extension, $name] = explode('/', $library, 2);
      $definition = $this->libraryDiscovery->getLibraryByName($extension, $name);
      if (isset($definition['css'])) {
        foreach ($definition['css'] as $options) {
          $options += $default_options;
          $options['browsers'] += [
            'IE' => TRUE,
            '!IE' => TRUE,
          ];

          // Files with a query string cannot be preprocessed.
          if ($options['type'] === 'file' && $options['preprocess'] && strpos($options['data'], '?') !== FALSE) {
            $options['preprocess'] = FALSE;
          }

          if ($library !== 'css_layers/css-layers-global') {
            if ($options['weight'] < CSS_LAYOUT) {
              $options['layer'] = 'base';
            }
            elseif ($options['weight'] < CSS_COMPONENT) {
              $options['layer'] = 'layout';
            }
            elseif ($options['weight'] < CSS_STATE) {
              $options['layer'] = 'component';
            }
            elseif ($options['weight'] < CSS_THEME) {
              $options['layer'] = 'state';
            }
            else {
              // CSS_THEME
              $options['layer'] = 'theme';
            }
          }

          // Always add a tiny value to the weight, to conserve the insertion
          // order.
          $options['weight'] += count($css) / 1000;

          // CSS files are being keyed by the full path.
          $css[$options['data']] = $options;
        }
      }
    }

    // Allow modules and themes to alter the CSS assets.
    $this->moduleHandler->alter('css', $css, $assets);
    $this->themeManager->alter('css', $css, $assets);

    // Sort CSS items, so that they appear in the correct order.
    uasort($css, 'static::sort');

    // Allow themes to remove CSS files by CSS files full path and file name.
    // @todo Remove in Drupal 9.0.x.
    if ($stylesheet_remove = $theme_info->getStyleSheetsRemove()) {
      foreach ($css as $key => $options) {
        if (isset($stylesheet_remove[$key])) {
          unset($css[$key]);
        }
      }
    }

    if ($optimize) {
      $css = \Drupal::service('asset.css.collection_optimizer')->optimize($css);
    }
    $this->cache->set($cid, $css, CacheBackendInterface::CACHE_PERMANENT, ['library_info']);

    return $css;
  }
}
