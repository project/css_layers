<?php

namespace Drupal\css_layers;

use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;

class CssLayersServiceProvider extends ServiceProviderBase implements ServiceProviderInterface {
  public function alter(ContainerBuilder $container) {
    $resolver = $container->getDefinition('asset.resolver');
    $resolver->setClass('Drupal\css_layers\CssLayersAssetResolver');

    $renderer = $container->getDefinition('asset.css.collection_renderer');
    $renderer->setClass('Drupal\css_layers\CssLayersAssetCssCollectionRenderer');

    // asset.css.collection_renderer
  }
}
